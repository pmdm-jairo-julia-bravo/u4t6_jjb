package com.androidjairo.u4t6contacts;

import android.net.Uri;

public class ContactItem {

    public String id;
    public String nombre;
    public String numero;
    public Uri imagen;
    public String look;
    public String raw;
    public String phone;
    public String _id;

    public ContactItem (String id, String nombre, String numero, Uri imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
    }

    //TODO constructor nuevo para utilizar todos los elementos
    public ContactItem (String id, String nombre, String numero, Uri imagen,String look, String raw, String phone, String _id ) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
        this.look = look;
        this.raw = raw;
        this.phone = phone;
        this._id = _id;
    }

    public String getLook() {
        return look;
    }

    public void setLook(String look) {
        this.look = look;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_Id() {
        return id;
    }

    public void set_Id(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Uri getImagen() {
        return imagen;
    }

    public void setImagen(Uri imagen) {
        this.imagen = imagen;
    }

    //TODO Ex10.2 Creamos un toString para pasarle este texto al TextView.

    @Override
    public String toString() {
        return  nombre + " " +
                numero + " " +
                "(" + phone + ")\n" +
                "_ID" + ":" + _id +
                "CONTACT_ID: " + id +
                "RAW_CONTACT_ID:" + raw +
                "\nLOOKUP_KEY:" + look;
    }
}
