package com.androidjairo.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener, MyAdapter.OnItemLongClickListener {

    MyContacts myContacts;
    ArrayList<ContactItem> Contactos;
    RecyclerView recyclerView;

    //Permissions required to contacts provider, only needed to read
    private static String[] PERMISSIONS_CONTACTS = {Manifest
            .permission.READ_CONTACTS};

    // Id to identify a contacts permission request
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if(checkPermisions())
            setListAdapter();
    }

    public void setUI(){

        myContacts =  new MyContacts(this);
        Contactos = myContacts.getContacts();
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        //set recyclerView with a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(this, RecyclerView.VERTICAL,false).getOrientation()));
    }

    private void setListAdapter(){
        //MyContacts class gets data from ContactsProvider
        myContacts = new MyContacts(this);
        //set adapter to recyclerView

        // TODO Ex10.2 le pasamos al constructor los dos listener que se relizaran cuando nosotros hagamos la pulsacion
        recyclerView.setAdapter(new MyAdapter(Contactos, this, this));

        //TODO Ex10.2 Cuando hagamos scroll en la aplicacion el textview pasara a ser invisible
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                TextView tvInfoContacto = findViewById(R.id.tvInfoContacto);

                tvInfoContacto.setVisibility(View.INVISIBLE);
            }
        });
        //Hide empty list TextView
        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
    }

    private boolean checkPermisions(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,MainActivity.PERMISSIONS_CONTACTS,MainActivity.REQUEST_CONTACTS);
            return false;
        }
        else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CONTACTS){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setListAdapter();
            else
                Toast.makeText(this,getString(R.string.contacts_read_right_required),Toast.LENGTH_LONG).show();
        }
        else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //TODO Ex10.2 Cuando hagamos una pulsacion sobre un contacto mostraremos el tv con los valores obtenidos
    @Override
    public void onItemClick(ContactItem item) {

        TextView tvInfoContacto = findViewById(R.id.tvInfoContacto);

        tvInfoContacto.setVisibility(View.VISIBLE);
        tvInfoContacto.setText(item.toString());
    }

    //TODO Ex10.2 Cuando hagamos una pulsacion larga lazamos un intent para abrir el editor de contactos
    public boolean onItemLongClick(ContactItem item) {

        Intent intent = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.getLookupUri(Long.parseLong(item.getId()),item.getLook()));

        /*
        Pasamos el intent y un codigo para recogerlo posteriormente
         */
        startActivityForResult(intent, 1);
        return true;
    }


    //TODO Ex10.2 Con este metodo actualizamos la vista al modificar un contacto.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ((MyAdapter)this.recyclerView.getAdapter()).resetContacts(myContacts.setListaContactos());
    }
}
