package com.androidjairo.u4t6contacts;


import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<ContactItem> listaContactos;

    //TODO Ex10.2 Declaramos los dos listener
    private OnItemClickListener listener;
    private OnItemLongClickListener listenerLong;


    //TODO Ex10.2 Creamos dos interface que tendran los metodos de pulsacion y p.larga

    public interface OnItemClickListener {

        void onItemClick(ContactItem item);
    }

    public interface OnItemLongClickListener {

        boolean onItemLongClick(ContactItem item);
    }

    //Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView id;
        TextView nombre;
        TextView num;
        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            this.id = view.findViewById(R.id.tvID);
            this.nombre = view.findViewById(R.id.tvNombre);
            this.num = view.findViewById(R.id.tvNumero);
            this.image = view.findViewById(R.id.imagenContacto);
        }

        //TODO Ex 10.2 Implementamos los metodos de pulsacion y pulsacion larga

        //sets viewHolder vews with data
        public void bind(final ContactItem item, final OnItemClickListener listener, final OnItemLongClickListener listenerLong) {

            this.id.setText(item.getId());
            this.nombre.setText(item.getNombre());
            this.num.setText(item.getNumero());
            this.image.setImageURI(item.getImagen());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return listenerLong.onItemLongClick(item);
                }
            });
        }
    }

    //TODO Ex 10.2 Ahora tenemos que pasarle al contructor los dos listener

    MyAdapter(ArrayList<ContactItem> Contactos, OnItemClickListener listener, OnItemLongClickListener listenerLong) {
        this.listaContactos = Contactos;
        this.listener = listener;
        this.listenerLong = listenerLong;
    }

    //Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Create item View:
        //use a simple TextView predefined layout (sdk/platforms/android-xx/data/res/layout) that contains only Textview
        ConstraintLayout cl = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items, parent, false);
        return new MyViewHolder(cl);
    }

    // replaces the data content of a viewholder (recycles old viewholder): Loyout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder viewHolder, int position) {
        //bind viewHolder with data at: position
        viewHolder.bind(listaContactos.get(position), listener, listenerLong);
    }

    // returns the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return listaContactos.size();
    }

    //TODO Ex10.2 Metodo para resetear la lista de contactos
    public void resetContacts(ArrayList<ContactItem> listaCont) {

        this.listaContactos = listaCont;

        notifyDataSetChanged();
    }
}