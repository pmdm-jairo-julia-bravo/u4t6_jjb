package com.androidjairo.u4t6contacts;


import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {
    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context){
        this.context = context;
        this.myDataSet = getContacts();
    }

    //Get contacts list from ContactsProvider
    public ArrayList<ContactItem> getContacts(){
        ArrayList<ContactItem> contactsList = new ArrayList<>();

        //acces to ContentProviders
        ContentResolver contentResolver = context.getContentResolver();

        //aux variables
        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI};

        String selectionFilter = ContactsContract.Data.MIMETYPE +"='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        //query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME+ " ASC");

        if (contactsCursor != null){
            //get the column indexes for desired Name and Number columns

            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int rawIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int photoIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);
            int _id = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);

            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);


            //read data and add to ArrayList
            while ((contactsCursor.moveToNext())){

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String id = contactsCursor.getString(idIndex);
                String look = contactsCursor.getString(lookIndex);
                String raw = contactsCursor.getString(rawIndex);
                String photo = contactsCursor.getString(photoIndex);
                String _ID = contactsCursor.getString(_id);
                String phoneType;
                Uri photoContact;

                if(photo != null){
                    photoContact = Uri.parse(photo);
                }else{

                    Resources resources = context.getResources();
                    photoContact = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + resources.getResourcePackageName(R.drawable.pikachu) +
                            '/' + resources.getResourceTypeName(R.drawable.pikachu) +
                            '/' + resources.getResourceEntryName(R.drawable.pikachu) );
                }

                //TODO Ex10.2 Switch para mostrar el tipo de contacto que es.
                /*
                Utilizo las constantes que proporciona android en vez de utilizar numeros.
                 */
                switch(typeIndex){

                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        phoneType = "HOME";
                        break;

                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        phoneType = "WORK";
                        break;

                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        phoneType = "MOBILE";
                        break;

                    default:
                        phoneType = "OTHER";
                        break;
                }

                //TODO Ex10.2 anyadimos el resto de elementos que no utilizabamos.
                contactsList.add(new ContactItem(id,name,number,photoContact,look, raw, phoneType, _ID));
            }
            contactsCursor.close();
        }
        return contactsList;
    }

    public ContactItem getContactData(int position){
        return myDataSet.get(position);
    }

    public int getCount(){
        return myDataSet.size();
    }

    // TODO Ex10.2 metodo para cargar el nuevo array de contactos y mostrar la información correcta cuando se edite un contacto.

    public ArrayList<ContactItem> setListaContactos() {
        this.myDataSet = getContacts();
        return this.myDataSet;
    }
}