package com.androidjairo.u4t6contacts;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {
    private ArrayList<String> myDataSet;
    private Context context;

    public MyContacts(Context context){
        this.context = context;
        this.myDataSet = getContacts();
    }

    //Get contacts list from ContactsProvider
    private ArrayList<String> getContacts(){
        ArrayList<String> contactsList = new ArrayList<>();

        //acces to ContentProviders
        ContentResolver contentResolver = context.getContentResolver();

        //aux variables
        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI};

        String selectionFilter = ContactsContract.Data.MIMETYPE +"='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        //query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME+ " ASC");

        if (contactsCursor != null){
            //get the column indexes for desired Name and Number columns

            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int rawIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int phoneIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int photoIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);

            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);


            //read data and add to ArrayList
            while ((contactsCursor.moveToNext())){

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String id = contactsCursor.getString(idIndex);
                String look = contactsCursor.getString(lookIndex);
                String raw = contactsCursor.getString(rawIndex);
                String phone = contactsCursor.getString(phoneIndex);
                String photo = contactsCursor.getString(photoIndex);


                contactsList.add(name + ": "+number + ": "+ id + ": "+look + ": "+ raw + ": "+ phone + ": "+ photo);
            }
            contactsCursor.close();
        }
        return contactsList;
    }

    public String getContactData(int position){
        return myDataSet.get(position);
    }

    public int getCount(){
        return myDataSet.size();
    }
}