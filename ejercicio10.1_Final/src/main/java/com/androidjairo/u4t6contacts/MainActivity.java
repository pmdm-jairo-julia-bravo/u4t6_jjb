package com.androidjairo.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MyContacts myContacts;
    ArrayList<item> Contactos;
    RecyclerView recyclerView;

    //Permissions required to contacts provider, only needed to read
    private static String[] PERMISSIONS_CONTACTS = {Manifest
            .permission.READ_CONTACTS};

    // Id to identify a contacts permission request
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if(checkPermisions())
            setListAdapter();
    }

    public void setUI(){

        myContacts =  new MyContacts(this);
        Contactos = myContacts.getContacts();
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        //set recyclerView with a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));

        //TODO EX 10.1 Monstamos una separacion entre filas
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(this, RecyclerView.VERTICAL,false).getOrientation()));
    }

    private void setListAdapter(){
        //MyContacts class gets data from ContactsProvider
        myContacts = new MyContacts(this);
        //set adapter to recyclerView
        recyclerView.setAdapter(new MyAdapter(Contactos));

        //Hide empty list TextView
        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
    }

    private boolean checkPermisions(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,MainActivity.PERMISSIONS_CONTACTS,MainActivity.REQUEST_CONTACTS);
            return false;
        }
        else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CONTACTS){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setListAdapter();
            else
                Toast.makeText(this,getString(R.string.contacts_read_right_required),Toast.LENGTH_LONG).show();
        }
        else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
