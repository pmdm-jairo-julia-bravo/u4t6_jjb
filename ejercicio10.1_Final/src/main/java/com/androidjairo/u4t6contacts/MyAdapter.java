package com.androidjairo.u4t6contacts;


import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    ArrayList<item> listaContactos;

    //Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder{

        //TODO Ex10.1 Nuevos atributos para poder pasar la informacion a la vista
        TextView id;
        TextView nombre;
        TextView num;
        ImageView image;


        public MyViewHolder(View view){
            super(view);
            this.id = view.findViewById(R.id.tvID);
            this.nombre = view.findViewById(R.id.tvNombre);
            this.num = view.findViewById(R.id.tvNumero);
            this.image = view.findViewById(R.id.imagenContacto);
        }

        //sets viewHolder vews with data
        public void bind(item item){

            /*
            Pasamos la informacion del contaco a los text view
             */
            this.id.setText(item.getId());
            this.nombre.setText(item.getNombre());
            this.num.setText(item.getNumero());
            this.image.setImageURI(item.getImagen());
        }
    }

    //constructor: mycontacts cntains Contacs data
    MyAdapter(ArrayList<item> Contactos){
        this.listaContactos = Contactos;
    }

    //Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Create item View:
        //use a simple TextView predefined layout (sdk/platforms/android-xx/data/res/layout) that contains only Textview
        ConstraintLayout cl = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items,parent,false);
        return new MyViewHolder(cl);
    }

    // replaces the data content of a viewholder (recycles old viewholder): Loyout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder viewHolder, int position) {
        //bind viewHolder with data at: position
        viewHolder.bind(listaContactos.get(position));
    }

    // returns the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return listaContactos.size();
    }
}