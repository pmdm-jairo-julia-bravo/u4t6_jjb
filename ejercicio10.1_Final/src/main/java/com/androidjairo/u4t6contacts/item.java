package com.androidjairo.u4t6contacts;

import android.net.Uri;

//TODO Ex 10.1 Nueva clase de Contacto en el cual vamos a guardar la informacion de este.
public class item {

    public String id;
    public String nombre;
    public String numero;
    public Uri imagen;

    public item(String id, String nombre, String numero, Uri imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Uri getImagen() {
        return imagen;
    }

    public void setImagen(Uri imagen) {
        this.imagen = imagen;
    }
}
